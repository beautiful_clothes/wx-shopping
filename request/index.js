// 同时发送异步代码的次数  解决多次请求下加载效果
let ajaxTimes = 0;
export const request = (params) => {
  // 判断url中是否有/my/ 有则带上header token
  let header = { ...params.header};
  if(params.url.includes("/my/")) {
    // 拼接header 带上token
    header["Authorization"] = wx.getStorage("token");
  }
    ajaxTimes++;
    // 显示加载中效果
    wx.showLoading({
        title: '加载中',
        // 蒙版
        mask:true
      })

    // 定义公共的url
  const baseUrl = "https://api-hmugo-web.itheima.net/api/public/v1"
    return new Promise((resolve, reject) => {
        wx.request({
            ...params,
            header: header,
            url:baseUrl+params.url,
            success:(result) => {
                resolve(result.data.message)
            },
            fail:(err) => {
                reject(err)
            },
            // 不管成功还是失败都会调用
            complete:() => {
                ajaxTimes--
                if(ajaxTimes === 0) {
                    // 关闭正在加载中效果
                    wx.hideLoading();
                }                
            }
        })
    })
}