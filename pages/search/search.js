/* 
1 输入框绑定 值改变事件 input事件
  1 获取到输入框的值
  2 合法性判断 
  3 检验通过 把输入框的值 发送到后台
  4 返回的数据打印到页面上
2 防抖 （防止抖动） 定时器  节流 
  0 防抖 一般 输入框中 防止重复输入 重复发送请求
  1 节流 一般是用在页面下拉和上拉 
  1 定义全局的定时器id
 */
import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goods:[],
    // 搭配hidden频繁管理取消按钮的显示与隐藏 hidden=true时隐藏
    isFocus:false,
    // 输入框的值
    inpValue:""
  },
  TimeId:-1,
  // 搜索框输入事件
  handleInput(e) {
    // 获取输入框的值
    const {value} = e.detail;
    // 检测合法性 去掉两边的空格
    if (!value.trim()) {
      // 不合法
      // 将取消按钮隐藏
      this.setData({
        goods:[],
        isFocus: false
      })
      return;
    } else {
      // 将取消按钮显示出来
      this.setData({
        isFocus:true
      })
      // 防抖功能 防止用户每输入一个字符就发送一次请求 开启定时器 先清除上次的定时器 再开启新的定时器
      clearTimeout(this.TimeId);
      this.TimeId = setTimeout(() => {
        // 发送请求拿到数据
        this.getInputResult(value);
      },1000);      
    }
  },
  // 发送请求获取搜索到的数据
  async getInputResult(query) {
    const res = await request({
      url: '/goods/qsearch',
      data: {query}
    })
    // console.log(res)
    this.setData({
      goods:res
    })
  },

  // 取消事件
  handleCancel() {
    this.setData({
      inpValue:"",
      isFocus:false,
      goods:[]
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  }
})