// pages/order/order.js
// 引入用来发送请求的方法  一定要把路径补全
import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orders:{},
    tabs: [{
      id: 0,
      value: "全部",
      isActive: true
    },
    {
      id: 1,
      value: "待付款",
      isActive: false
    },
    {
      id: 2,
      value: "待发货",
      isActive: false
      },
      {
        id: 3,
        value: "退款/退货",
        isActive: false
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  // 获取订单列表的方法
  async getOrders(type) {
    const res = await wx.request({
      url: '/my/orders/all',
      data: '{type}',
      method: 'GET'
    })
    this.setData({
      // 对时间进行处理
      orders: res.orders.map(v => ({ ...v, create_time_cn: (new Date(v.create_time * 1000).toLocaleString())}))
    })
  },

  // 根据标题索引改变激活选项
  changeTitleIndex(index) {
    // 修改数组的激活状态
    let { tabs } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    // 赋值到DATA中
    this.setData({
      tabs
    })
  },
  // 标题的点击事件 是从子组件传过来的自定义方法
  handleTabsItemChange(e) {
    // 获取点击的标题索引
    const { index } = e.detail;
    this.changeTitleIndex(index);
    // index改变后重新发送请求 type=index+1
    this.getOrders(index+1);
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow(options) {
    // 判断有没有授权
    const token = wx.getStorageSync("token")
    if(!token) {
      wx.navigateTo({
        url: '/pages/auth/auth',
      });
      return;
    }
    // 获取小程序的页面栈-数组 max(10)页面 
    let pages = getCurrentPages();
    // 数组中索引最大的页面就是当前页面
    let currentPage = pages[pages.length-1];
    // console.log(currentPage.options);
    // 获取url上的type参数
    const {type} = currentPage.options
    // 改变激活选项 index = type-1
    this.changeTitleIndex(type - 1);
    // 发送请求获取数据
    this.getOrders(type);
  }
})