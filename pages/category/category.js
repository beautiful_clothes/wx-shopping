// 引入用来发送请求的方法  一定要把路径补全
import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
// pages/category/category.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 左侧菜单数据
    leftCatesList:[],
    rightCatesList:[],
    // 被点击的左侧菜单
    currentIndex:0,
    // 右侧内容滚动条距离上面的距离
    scrollTop:0
  },

  // 接口的返回数据
  Cates:[],

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 先判断本地有没有旧的数据 有并且没有过期就直接渲染 没有再发送请求获取
    const Cates = wx.getStorageSync("cates");
    if(!Cates) {
      this.getCates();
    } else {
      // 判断有没有过期 定义过期时间
      if(Date.now() - Cates.time>1000*10) {
        this.getCates();
      } else {
        // 可以使用旧数据
        this.Cates = Cates.data;
         // 构造左侧大菜单数据
        let leftCatesList = this.Cates.map(v => v.cat_name);
        // 构造右侧商品数据
        let rightCatesList = this.Cates[0].children;
        this.setData({
          leftCatesList,
          rightCatesList
        })
      }
    }

    // this.getCates()
  },
  // 获取分类数据
  async getCates() {
    // request({
    //   url:"/categories"
    // })
    // .then(res => {
    //   this.Cates = res.data.message
    //   // 把接口返回来的数据存储到本地存储中
    //   wx.setStorageSync("cates", {time:Date.now(), data:this.Cates});
    //   // 构造左侧大菜单数据
    //   let leftCatesList = this.Cates.map(v => v.cat_name);
    //   // 构造右侧商品数据
    //   let rightCatesList = this.Cates[0].children;
    //   this.setData({
    //     leftCatesList,
    //     rightCatesList
    //   })
    // })

    // 使用ES7发送请求
    const res = await request({url:"/categories"});
    this.Cates = res
      // 把接口返回来的数据存储到本地存储中
      wx.setStorageSync("cates", {time:Date.now(), data:this.Cates});
      // 构造左侧大菜单数据
      let leftCatesList = this.Cates.map(v => v.cat_name);
      // 构造右侧商品数据
      let rightCatesList = this.Cates[0].children;
      this.setData({
        leftCatesList,
        rightCatesList
      })
  },

  // 左侧菜单点击事件
  handleItemTap(e) {
    // 获取被点击的索引 给data中的数据赋值 根据不同的索引渲染页面
    const {index} = e.currentTarget.dataset;
    // 构造右侧商品数据
    let rightCatesList = this.Cates[index].children;    
    this.setData({
      currentIndex:index,
      rightCatesList,
      // 重新设置右侧内容的scroll-view标签距离顶部的距离
      scrollTop:0   
    })  
   
  }
})