/*
微信支付
  1 哪些人 哪些帐号 可以实现微信支付
    1 企业帐号 
    2 企业帐号的小程序后台中 必须 给开发者 添加上白名单 
      1 一个 appid 可以同时绑定多个开发者
      2 这些开发者就可以公用这个appid 和 它的开发权限  
3 支付按钮
  1 先判断缓存中有没有token
  2 没有 跳转到授权页面 进行获取token 
  3 有token 。。。
  4 创建订单 获取订单编号
  5 已经完成了微信支付
  6 手动删除缓存中 已经被选中了的商品 
  7 删除后的购物车数据 填充回缓存
  8 再跳转页面 
 */
// 引入用来发送请求的方法  一定要把路径补全
import { getSetting, chooseAddress, openSetting, showModal, showToast, requestPayment } from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
import{   } from "../../request/index.js";
Page({
    // 点击添加收货地址事件 API获取收货地址
  // 获取用户对小程序所授予的获取地址的权限状态 scope为true或false 
  // authSetting  scope.address
  // 如果用户从未点击过获取收获地址按钮 scope为undefined

  // scope为true或undefined都可以调用获取收货地址权限
  // scope为false时诱导用户自己打开授权设置页面 重新授权 wx.openSetting
  // 把获取到的收货地址存到本地存储中
  async handleChooseAddress() {    
    try{
    // 获取用户对小程序所授予的获取地址的权限状态 scope
    // wx.getSetting({
    //   success: (result)=>{
    //     // 获取权限状态  属性名很怪异时使用[]形式获取属性值
    //     const scopeAddress = result.authSetting['scope.address']
    //     if(scopeAddress === true || scopeAddress === undefined) {
    //       wx.chooseAddress({
    //         success: (result1)=>{             
    //         }
    //       });
    //     } else {
    //       // 用户取消了授予收货地址权限 
    //       // scope为false时诱导用户自己打开授权设置页面 重新授权
    //       wx.openSetting({
    //         success: (result2)=>{
    //           // 调用获取收货地址代码
    //           wx.chooseAddress({
    //             success: (result3)=>{                  
    //             }
    //           });
    //         }
    //       });
    //     }
    //   }
    // });

    // 获取权限状态
    const res1 = await getSetting();
    // 获取权限状态  属性名很怪异时使用[]形式获取属性值
    const scopeAddress = res1.authSetting['scope.address'];
    if(scopeAddress === false) {      
      // scope为false时诱导用户自己打开授权设置页面 重新授权
      await openSetting();
    }
    // 调用获取收货地址代码API
    const address = await chooseAddress();
    address.all = address.provinceName+address.cityName+address.countyName+address.detailInfo;
    // 把获取到的收货地址存到本地存储中
    wx.setStorageSync("address",address)
  } catch(error) {
    console.log(error);
  }
  },
  /**
   * 页面的初始数据
   */
  data: {
    address:{},
    cart:[],
    totalPrice:0,
    totalNum:0
  },

   /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取本地存储中的地址数据
    const address = wx.getStorageSync("address");
    // 获取缓存中的购物车数据
    let cart = wx.getStorageSync("cart") || [];
    // 根据购物车中的商品数据 checked为true的数据才是用户要支付的商品
    // 过滤 checked为true的数据
    cart = cart.filter(v=>v.checked);
    // 根据购物车中的商品数据 所有的商品都被选中时全选才被选中
    // 计算全选  every方法遍历数组 接受一个回调函数
    // 当每个回调函数都返回true时 every方法的返回值才为true
    // 空数组调用every方法返回值也是true
    // let allChecked = cart.length?cart.every(v=>v.checked):false;
    this.setData({address});
    // 重新计算价格 全选 总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      totalPrice+=v.num * v.goods_price;
      totalNum+=v.num;
    })
    // 重新填充到data和缓存中
    this.setData({
      cart,
      totalPrice,
      totalNum,
      address
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  async handleOrderPay() {
    try {
      // 判断缓存中是否有token
      const token = wx.getStorageSync("token");
      if (!token) {
        // 跳转到授权页面
        wx.navigateTo({
          url: '/pages/auth/auth',
        });
      }
      // 已经存在token  创建订单  请求头参数
      // const header = { Authorization: token };
      // 请求体参数
      const order_price = this.data.totalPrice;
      const consignee_addr = this.data.address.all;
      const cart = this.data.cart;
      let goods = [];
      cart.forEach(v => goods.push({
        goods_id: v.goods_id,
        goods_number: v.num,
        goods_price: v.goods_price
      }))
      const orderParams = { order_price, consignee_addr, goods };
      // 发送请求 创建订单 获取订单编号order_number
      const { order_number } = await wx.request({
        url: '/my/orders/create',
        data: orderParams,
        header: header,
        method: 'POST'
      });
      // 获取支付参数timeStamp nonceStr package signType paySign
      const { pay } = await wx.request({
        url: '/my/orders/req_unifiedorder',
        data: { order_number },
        header,
        method: 'POST'
      })
      // 发送微信支付requestPayment
      await wx.requestPayment(pay);
      // 查询订单状态
      const res = await wx.request({
        url: '/my/orders/chkOrder',
        data: { order_number },
        header,
        method: 'POST'
      })
      // 弹框提示支付结果 成功或失败
      wx.showToast({
        title: '支付成功！',
      });
      // 手动删除缓存中 已经被选中了的商品
      let newCart = wx.getStorage("cart");
      // 过滤缓存中已经被选中的数据 留下未被选中的数据
      newCart = newCart.filter(v => !v.checked);
      // 将数据重新填充回缓存中
      wx.setStorage("cart", newCart);
      // 跳转到订单页面
      wx.navigateTo({
        url: '/pages/order/order',
      });
    } catch (error) {
      // 弹框提示支付结果 成功或失败
      wx.showToast({
        title: '支付失败！',
      })
    }
  }   
  })