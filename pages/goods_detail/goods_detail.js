/* 
1 发送请求获取数据 
2 点击轮播图 预览大图
  1 给轮播图绑定点击事件
  2 调用小程序的api  previewImage 
3 点击 加入购物车
  1 先绑定点击事件
  2 获取缓存中的购物车数据 数组格式 
  3 先判断 当前的商品是否已经存在于 购物车
  4 已经存在 修改商品数据  执行购物车数量++ 重新把购物车数组 填充回缓存中
  5 不存在于购物车的数组中 直接给购物车数组添加一个新元素 新元素 带上 购买数量属性 num  重新把购物车数组 填充回缓存中
  6 弹出提示
4 商品收藏
  1 页面onShow的时候  加载缓存中的商品收藏的数据
  2 判断当前商品是不是被收藏 
    1 是 改变页面的图标
    2 不是 。。
  3 点击商品收藏按钮 
    1 判断该商品是否存在于缓存数组中
    2 已经存在 把该商品删除
    3 没有存在 把商品添加到收藏数组中 存入到缓存中即可
 */

// 引入用来发送请求的方法  一定要把路径补全
import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsObj:{},
    // 商品是否被收藏过
    isCollect: false
  },

  // 商品对象
  GoodsInfo:{},

  /**
   * 生命周期函数--监听页面加载
   * onShow中是拿不到options的
   * 只有在onLoade中才能得到options
   * 在onShow中想要拿到options 只有得到页面栈
   */
  onShow: function () {
    // 获取小程序的页面栈-数组 max(10)页面 
    let pages = getCurrentPages();
    // 数组中索引最大的页面就是当前页面
    let currentPage = pages[pages.length - 1];
    // console.log(currentPage.options);
    const options = currentPage.options;

    const {goods_id} = options;
    this.getGoodsDetail(goods_id);
  },

  // 获取商品详情数据
  async getGoodsDetail(goods_id) {
    const goodsObj = await request({url:"/goods/detail",data:{goods_id}});
    this.GoodsInfo = goodsObj;
    // 拿到缓存中的商品收藏数组
    let collect = wx.getStorageSync("collect") || [];
    // 判断商品是否被收藏
    let isCollect = collect.some(v => v.goods_id === this.GoodsInfo.goods_id);

    this.setData({
      goodsObj: {
        goods_price:goodsObj.goods_price,
        goods_name:goodsObj.goods_name,
        // iphone部分手机不识别Webp图片格式  
        // 前端最好找后台进行修改  
        // 自己改要确保后台存在jpg格式的图片  
        // 正则表达式找到。webp存在的地方修改成。jpg
        goods_introduce:goodsObj.goods_introduce.replace(/\.webp/g,'.jpg'),        
        pics:goodsObj.pics
      },
      isCollect
    })
  },

  // 点击轮播图预览大图
  // 给轮播图绑定点击事件 在进行预览  调用了小程序的API  PrevewImage
  handlePrevewImage(e) {
    // 构造新数组
    const urls = this.GoodsInfo.pics.map(v=>v.pics_mid);
    // 接受点击图片传递过来的url
    const current = e.currentTarget.dataset.url;
    wx.previewImage({
      current, // 当前显示图片的http链接
      urls // 需要预览的图片http链接列表
    })
  },

  // 商品收藏事件
  handleCollect() {
    let isCollect = false;
    // 获取缓存中的商品收藏数组
    let collect=wx.getStorageSync("collect")||[];
    // 判断商品是否被收藏过
    let index = collect.findIndex(v=>v.goods_id===this.GoodsInfo.goods_id);
    // 当index!=-1 表示收藏过
    if(index!==-1) {
      // 在数组中删除该商品
      collect.splice(index,1);
      // 收藏状态改为false
      isCollect = false;
      wx.showToast({
        title: '取消成功！',
        icon:'success',
        mask: true
      })
    } else {
      // 在数组中添加该商品
      collect.push(this.GoodsInfo);
      isCollect = true;
      wx.showToast({
        title: '收藏成功！',
        icon: 'success',
        mask: true
      })
    }
    // 把数组存入到缓存中
    wx.setStorageSync("collect",collect);
    // 修改data中的属性
    this.setData({
      isCollect
    })
  },

  // 点击加入购物车 点击事件
  // 获取缓存中的购物车数据
  // 先判断当前商品是否已经存在在购物车中
  // 在的话只增加数量 重新把购物车数组缓存
  // 不存在直接给购物车数组添加一个新元素 数量
  // 然后缓存 弹出提示
  handleCartAdd() {
    // 获取缓存中的购物车数据
    let cart = wx.getStorageSync("cart") || [];
    // 先判断当前商品是否已经存在在购物车中
    let index = cart.findIndex(v=>v.goods_id === this.GoodsInfo.goods_id);
    if(index === -1) {
      // 不存在直接给购物车数组添加一个新元素 数量 选中状态
      this.GoodsInfo.num = 1;
      this.GoodsInfo.checked = true;
      cart.push(this.GoodsInfo);
    } else {
      // 在的话只增加数量 重新把购物车数组缓存 num++
      cart[index].num++;
    }
    // 然后缓存 弹出提示
    wx.setStorageSync("cart", cart);
    wx.showToast({
      title: '加入购物车成功',
      icon: 'success',
      // 防止用户疯狂点击
      mask: true
    });
  }
})