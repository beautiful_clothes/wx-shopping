// pages/cart/cart.js
// 引入用来发送请求的方法  一定要把路径补全
import { getSetting, chooseAddress, openSetting, showModal, showToast } from "../../utils/asyncWx.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({
    // 点击添加收货地址事件 API获取收货地址
  // 获取用户对小程序所授予的获取地址的权限状态 scope为true或false 
  // authSetting  scope.address
  // 如果用户从未点击过获取收获地址按钮 scope为undefined

  // scope为true或undefined都可以调用获取收货地址权限
  // scope为false时诱导用户自己打开授权设置页面 重新授权 wx.openSetting
  // 把获取到的收货地址存到本地存储中
  async handleChooseAddress() {    
    try{
    // 获取用户对小程序所授予的获取地址的权限状态 scope
    // wx.getSetting({
    //   success: (result)=>{
    //     // 获取权限状态  属性名很怪异时使用[]形式获取属性值
    //     const scopeAddress = result.authSetting['scope.address']
    //     if(scopeAddress === true || scopeAddress === undefined) {
    //       wx.chooseAddress({
    //         success: (result1)=>{             
    //         }
    //       });
    //     } else {
    //       // 用户取消了授予收货地址权限 
    //       // scope为false时诱导用户自己打开授权设置页面 重新授权
    //       wx.openSetting({
    //         success: (result2)=>{
    //           // 调用获取收货地址代码
    //           wx.chooseAddress({
    //             success: (result3)=>{                  
    //             }
    //           });
    //         }
    //       });
    //     }
    //   }
    // });

    // 获取权限状态
    const res1 = await getSetting();
    // 获取权限状态  属性名很怪异时使用[]形式获取属性值
    const scopeAddress = res1.authSetting['scope.address'];
    if(scopeAddress === false) {      
      // scope为false时诱导用户自己打开授权设置页面 重新授权
      await openSetting();
    }
    // 调用获取收货地址代码API
    const address = await chooseAddress();
    address.all = address.provinceName+address.cityName+address.countyName+address.detailInfo;
    // 把获取到的收货地址存到本地存储中
    wx.setStorageSync("address",address)
  } catch(error) {
    console.log(error);
  }
  },
  /**
   * 页面的初始数据
   */
  data: {
    address:{},
    cart:[],
    allChecked:false,
    totalPrice:0,
    totalNum:0
  },

   /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取本地存储中的地址数据
    const address = wx.getStorageSync("address");
    // 获取缓存中的购物车数据
    const cart = wx.getStorageSync("cart") || [];
    // 根据购物车中的商品数据 所有的商品都被选中时全选才被选中
    // 计算全选  every方法遍历数组 接受一个回调函数
    // 当每个回调函数都返回true时 every方法的返回值才为true
    // 空数组调用every方法返回值也是true
    // let allChecked = cart.length?cart.every(v=>v.checked):false;
    this.setData({address});
    this.setCart(cart);
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  // 商品的选中事件
  // 获取被修改的商品对象  进行取反
  // 重新填充到data和缓存中
  // 重新计算价格 全选 总数量
  handleItemChange(e) {
    const goods_id = e.currentTarget.dataset.id;
    // 获取数组 和被修改的商品对象id  进行取反
    let {cart} = this.data;
    let index = cart.findIndex(v=>v.goods_id === goods_id);
    cart[index].checked = !cart[index].checked;
    this.setCart(cart);   
  },

  // 设置购物车状态  同事计算价格、全选、数量
  setCart(cart) {   
    // 重新计算价格 全选 总数量
    let allChecked = true;
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    })
    // 判断数组是为空
    allChecked = cart.length != 0 ? allChecked : false;
    // 重新填充到data和缓存中
    this.setData({
      cart,
      totalPrice,
      totalNum,
      allChecked
    });
    wx.setStorageSync("cart", cart);
  },
  
  // 全选和反选事件
  // 获取data中的allchecked
  // 直接取反 遍历购物车数组 让商品状态随着allchecked的改变而改变
  // 吧购物车数组和选中状态重新设置会缓存中
  handleItemAllCheck() {
    let {cart, allChecked } = this.data;
    allChecked = !allChecked;
    // 循环修改cart商品数组中的商品选中状态
    cart.forEach(v => v.checked = allChecked);
    this.setCart(cart);
  },

  // 商品数量的点击事件 自定义属性  +1  -1
  // 传递被点击的商品ID
  // 获取data中的购物车数组 
  // 当购物车的数量为1时同时用户点击减按钮，弹框提示showModal用户是否要删除商品
  // 直接修改商品对象的数量
  // 重新设置回缓存和data重中
  async handleItemNumEdit(e) {
    // 1 获取传递过来的参数 
    const { operation, id } = e.currentTarget.dataset;
    // 2 获取购物车数组
    let { cart } = this.data;
    // 3 找到需要修改的商品的索引
    const index = cart.findIndex(v => v.goods_id === id);
    // 4 判断是否要执行删除
    if (cart[index].num === 1 && operation === -1) {
      // 4.1 弹窗提示
      const res = await showModal({ content: "您是否要删除？" });
      if (res.confirm) {
        // 数组删除
        cart.splice(index, 1);
        this.setCart(cart);
      }
    } else {
      // 4  进行修改数量
      cart[index].num += operation;
      // 5 设置回缓存和data中
      this.setCart(cart);
    }
  },

  // 点击结算按钮事件
  // 有商品同时又收货地址才跳转到支付页面
  async handleItemToPay (e) {
    // 1 获取传递过来的参数 
    const {address,totalNum} = this.data;
    if(!address.userName) {
      await showToast({title:"您还没有选择收货地址！"});
      return;
    }
    if (totalNum===0) {
      await showToast({ title: "您还没有选购商品！" });
      return;
    }
    // 跳转到支付页面
    wx.navigateTo({
      url: '/pages/pay/pay'
    })
  }
})