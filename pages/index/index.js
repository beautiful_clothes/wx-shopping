// 引入用来发送请求的方法  一定要把路径补全
import { request } from "../../request/index.js";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperList:[],
    catesList:[],
    floorList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const that = this
  //   // 发送异步请求 优化promise
  //   wx.request({
  //     url: 'https://api.zbztb.cn/api/public/v1/home/swiperdata',
  //     method: 'GET',
  //     success: function(res) {
  //       that.setData({
  //         swiperList: res.data.message
  //       })
  //     }
  //   })

  that.getSwiperList();
  that.getCateList();
  that.getFloorList();
  
  },
  
  // 获取轮播图数据
  getSwiperList() {
    // 优化promise
    request({
      // 传递参数  丢到...params中
      url:"/home/swiperdata"})
      .then(res => {
        this.setData({
          swiperList: res
        })
      })
  },

  // 获取轮播图数据
  getCateList() {
      // 优化promise
    request({
      // 传递参数  丢到...params中
      url:"/home/catitems"})
      .then(res => {
        this.setData({
          catesList: res
        })
      })
    },

    // 获取轮播图数据
  getFloorList() {
    // 优化promise
    request({
      // 传递参数  丢到...params中
      url:"/home/floordata"})
      .then(res => {
        this.setData({
          floorList: res
        })
      })
    },
  handleClick(e) {
    console.log(e)
  }
  
})