// 引入用来发送请求的方法  一定要把路径补全
import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs:[{
      id:0,
      value:"綜合",
      isActive:true
    },
    {
      id:1,
      value:"銷量",
      isActive:false
    },
    {
      id:2,
      value:"價格",
      isActive:false
    }
  ],
  goodsList:[]
  },


  //全局参数

  // 接口传的参数
  queryParams:{
    query:"",
    cid:"",
    pagenum:1,
    pagesize:10
  },

  totalPages:1,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.queryParams.cid = options.cid || "";
    this.queryParams.query = options.query || "";
    this.getGoodsList();
  },

  // 获取商品列表数据
  async getGoodsList() {
    const res=await request({url:"/goods/search",data:this.queryParams});
    // 获取总条数
    const { total } = res;
    // 计算总页数
    this.totalPages = Math.ceil(total/this.queryParams.pagesize);
    this.setData({
      // 解构旧数组和新的数组 拼接数组
      goodsList:[...this.data.goodsList,...res.goods]
    })
  },
  
  // 标题的点击事件 是从子组件传过来的自定义方法
  handleTabsItemChange(e) {
    // 获取点击的标题索引
    const {index} = e.detail;
    // 修改数组的激活状态
    let {tabs} = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false );
    // 赋值到DATA中
    this.setData({
      tabs
    })
  },

    /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // 重置数据 数组 页码
    this.setData({
      goodsList:[]      
    })
    this.queryParams.pagenum=1;
    this.getGoodsList();
    // 数据请求回来后手动关闭下拉刷新窗口
    wx.wx.stopPullDownRefresh();
  },
    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    // 判断是否有下一页数据 总页数 = Math.ceil(总条数/pagesize)
    // 总页数 = Math.ceil(23/10) = 3
    // 当前页码pagenum是否大于等于总页数没有下一页了
    // 判断有没有下一页
    if(this.queryParams.pagenum>=this.totalPages) {
      // 没有下一页
      wx.showToast({
        title:'没有下一页数据了'
      })
    } else {
      // 页码++ 发送请求    将请求回来的数据拼接到数组中
      this.queryParams.pagenum++;
      this.getGoodsList();
    }
  }
})

