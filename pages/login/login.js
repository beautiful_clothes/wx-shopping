// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

// 获取用户信息
  handleGetUserInfo(e) {
    const {userInfo} = e.detail;
    // 将用户信息保存到缓存中
    wx.setStorageSync("userinfo", userInfo);
    // 跳转到上层页面
    wx.navigateBack({
      delta:1
    })
    }
})